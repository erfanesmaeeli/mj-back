from django.shortcuts import render

def index(request):
	return render(request , "index.html")


def product(request):
	return render(request , "product.html")

def product_detail(request):
	return render(request , "product_detail.html")

def contactus(request):
	return render(request , "contactus.html")

def signup_(request):
	return render(request , "signup.html")
