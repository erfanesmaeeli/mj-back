from django.contrib import admin
from django.urls import path, include
from app_main import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index , name = 'index'),
    path('product/', views.product , name = 'product'),
    path('product-detail/', views.product_detail , name = 'product-detail'),
    path('contactus/', views.contactus , name = 'contactus'),
    path('signup/', views.signup_ , name = 'signup'),



] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
